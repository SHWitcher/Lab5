package com.sh.a1339644.lab5;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;

/**
 * Created by 1339644 on 9/26/2016.
 */
public class Activity5InstructFinish extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_5_denied);
    }

    @Override
    public void finish()
    {
        Intent i = new Intent();
        i.putExtra("key","Dave. I'm afraid I can't do that.");
        setResult(RESULT_OK,i);
        super.finish();
    }
}

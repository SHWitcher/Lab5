package com.sh.a1339644.lab5;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
/**
 * Created by 1339644 on 9/26/2016.
 */
public class Activity2Number extends Activity {


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number2);
    }

    public void onReturnOne(View v)
    {
        Intent i = new Intent();
        i.putExtra("key","Activity 2: One,1, un/une! ");
        setResult(RESULT_OK,i);
        finish();
    }

    public void onReturnTwo(View v)
    {
        Intent i = new Intent();
        i.putExtra("key","Activity 2: Two,2, due/deux! ");
        setResult(RESULT_OK,i);
        finish();
    }
}

package com.sh.a1339644.lab5;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import android.app.Activity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void launchActivity2(View v)
    {
        Intent i = new Intent(this, Activity2Number.class);

        startActivity(i);
    }

    public void launchActivity3(View v)
    {
        Intent i = new Intent(this, Activity3Launcher4and5.class);

        startActivity(i);
    }

    public void launchBrowser(View v)
    {
        Uri uri = Uri.parse("http://thewitcher.com/en/witcher3");
        Intent i = new Intent(Intent.ACTION_VIEW,uri);
        startActivity(i);
    }

    public void launchGeo(View v)
    {
      Uri uri = Uri.parse("geo:0,0?q=North Korea");
       Intent i = new Intent(Intent.ACTION_VIEW,uri) ;
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int request, int result, Intent i)
    {
        String str="";
        TextView tv = (TextView)findViewById(R.id.presentText);
        tv.setVisibility(View.VISIBLE);

        if(i==null)
        {
            str="Nothing to show here.";
        }
        else
        {
            i.getExtras().getString("key");
        }
        tv.setText(String.format(getResources().getString(R.string.dispalyR),request,str));
    }
}

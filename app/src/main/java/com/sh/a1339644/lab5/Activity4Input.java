package com.sh.a1339644.lab5;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

/**
 * Created by 1339644 on 9/26/2016.
 */
public class Activity4Input extends Activity{
    @Override
    protected void onCreate(Bundle savedIS)
    {
        super.onCreate(savedIS);
        setContentView(R.layout.activiy_4_input);
    }

    public void onClick(View view)
    {
        Intent i = new Intent();

        String str = ((EditText)findViewById(R.id.editText)).getText().toString();
        if(str.equals(""))
            str="Nothing to show.";

        i.putExtra("key",str);

        setResult(RESULT_OK,i);
        finish();

    }


}
